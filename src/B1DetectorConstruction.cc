// My_example
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file B1DetectorConstruction.cc
/// \brief Implementation of the B1DetectorConstruction class

#include "B1DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4Tubs.hh"
#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4VSolid.hh"
#include "G4UnionSolid.hh"

B1DetectorConstruction::B1DetectorConstruction(): G4VUserDetectorConstruction()
{ }

B1DetectorConstruction::~B1DetectorConstruction()
{ }

G4VPhysicalVolume* B1DetectorConstruction::Construct()
{
  // Get nist material manager
  G4NistManager* nist = G4NistManager::Instance();



  // Option to switch on/off checking of volumes overlaps
  //
  G4bool checkOverlaps = true;

  constexpr G4bool writeGDML=true;

  //***********************************************
  // Material
  //***********************************************



  //Concrete


  G4double a,z,density,fractionmass;
  G4String name,symbol;
  G4int nel;

  a=16.*g/mole;
  G4Element* elO=new G4Element(name="Oxygen",symbol="O2",z=8.,a);

  a=28.085*g/mole;
  G4Element* elSi=new G4Element(name="Silicon",symbol="Si",z=14.,a);

  a=40.08*g/mole;
  G4Element* elCa=new G4Element(name="Calcium",symbol="Ca",z=20.,a);

  a=22.99*g/mole;
  G4Element* elNa=new G4Element(name="Sodium",symbol="Na",z=11.,a);

  a=55.850*g/mole;
  G4Element* elFe=new G4Element(name="Iron",symbol="Fe",z=26.,a);

  a=26.98*g/mole;
  G4Element* elAl=new G4Element(name="Aluminium",symbol="Al",z=13.,a);



  density = 2.5*g/cm3;
  G4Material* Concrete = new G4Material(name="Concrete",density,nel=6);
  Concrete->AddElement(elO, fractionmass = 0.52);
  Concrete->AddElement(elSi, fractionmass = 0.325);
  Concrete->AddElement(elCa, fractionmass = 0.06);
  Concrete->AddElement(elNa, fractionmass = 0.015);
  Concrete->AddElement(elFe, fractionmass = 0.04);
  Concrete->AddElement(elAl, fractionmass = 0.04);



  // Standard Rock

  a=  1.00794*g/mole;
  // G4Element* H = new G4Element(name="Hydrogen",   symbol="H",  z=1.,  a);
  a= 12.01   *g/mole;
  G4Element* C = new G4Element(name="C",          symbol="C",  z=6.,  a);
  a= 14.0067 *g/mole;
  // G4Element* N = new G4Element(name="Nitrogen",   symbol="N",  z=7.,  a);
  a= 15.9994 *g/mole;
  G4Element* O = new G4Element(name="Oxygen",     symbol="O",  z=8.,  a);
  a= 22.98976928*g/mole;
  // G4Element* Na = new G4Element(name="Sodium",    symbol="Na", z=11., a);
  a= 24.3050 *g/mole;
  G4Element* Mg = new G4Element(name="Magnesium", symbol="Mg", z=12., a);
  a= 35.4527 *g/mole;
  // G4Element* Cl = new G4Element(name="Chlorine",  symbol="Cl", z=17., a);
  a= 40.0784*g/mole;
  G4Element* Ca = new G4Element(name="Calcium",   symbol="Ca", z=20., a);
  a=  2.013553*g/mole;
  // G4Element* D = new G4Element(name="Deuterium",  symbol="D",  z=1.,  a);
  a= 17.99916*g/mole;
  // G4Element* O18 = new G4Element(name="IsoOxygen",symbol="O18",  z=8.,  a);


  G4Material* StdRock = new G4Material(name="StdRock",density=2.65*g/cm3,nel=4);
  StdRock->AddElement(O,  52.*perCent);
  StdRock->AddElement(Ca, 27.*perCent);
  StdRock->AddElement(C,  12.*perCent);
  StdRock->AddElement(Mg,  9.*perCent);


  G4Material* AIR = nist->FindOrBuildMaterial("G4_AIR");



  //************************************************************
  //COLOR
  //************************************************************

  G4Colour brown(0.7, 0.4, 0.1);
  G4VisAttributes* colorbrown = new G4VisAttributes(brown);


  //G4Colour green(0., 255., 0.);
  //G4VisAttributes* colorgreen = new G4VisAttributes(green);


  // G4Color red(1.0, 0.0, 0.0);
  // G4VisAttributes* colorred = new G4VisAttributes(red);

  //G4Color blue(0.0, 0.0, 1.0);
  //G4VisAttributes* colorblue = new G4VisAttributes(blue);


  // G4Color yellow(1.0, 1.0, 0.0);
  // G4VisAttributes* coloryellow = new G4VisAttributes(yellow);

  //G4Color cyan(0.0, 1.0, 1.0);
  //G4VisAttributes* colorcyan = new G4VisAttributes(cyan);

  // G4Color magenta(1.0, 0.0, 1.0);
  // G4VisAttributes* colormagenta = new G4VisAttributes(magenta);


  G4Color grey(0.5, 0.5, 0.5);
  G4VisAttributes* colorgray = new G4VisAttributes(grey);

  //*******************************
  // Create  World
  //*******************************

  G4double world_sizeX = 100*m;
  G4double world_sizeY = 100*m;
  G4double world_sizeZ = 100*m;



  // Create Solid
  G4Box* solidWorld =new G4Box("SolidWorld",world_sizeX,world_sizeY,world_sizeZ);

  // Create Logical
  G4LogicalVolume* logicWorld = new G4LogicalVolume(solidWorld,AIR,"LogicWorld");


  // Create Physical (Placement)

  G4VPhysicalVolume* physicalWorld = new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "PhysicalWorld",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      checkOverlaps);        //overlaps checking


  //*******************************
  // Create Rock
  //*******************************

  // Rock (92*92*92)

  G4double L_Rock=100.*m;
  G4double W_Rock=100.*m;
  G4double H_Rock=50.*m;


  // Create Solid
  G4Box* solidRock =new G4Box("SolidRock",L_Rock,W_Rock,H_Rock);

  //Create Logical
  G4LogicalVolume* logicRock = new G4LogicalVolume(solidRock,StdRock,"LogicRock");
  logicRock->SetVisAttributes(colorbrown);

  // Create Physical (Placement)

  //G4RotationMatrix* zRotRock = new G4RotationMatrix;
  // zRotRock->rotateZ(M_PI*rad);

  G4VPhysicalVolume* physicalRock = nullptr;
  physicalRock =  new G4PVPlacement(0,
                      G4ThreeVector(0.,0.,-H_Rock),       
                      logicRock,            //its logical volume
                      "PhysicalRock",               //its name
                      logicWorld,           //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      checkOverlaps);        //overlaps checking


  //*******************************
  // Create Cavern
  //*******************************

  //Cavern (53*30*35)



  G4double L_cavern=53.*m;
  G4double W_cavern=30.*m;
  G4double H_cavern=35.*m;

  G4double Rmax_SHAFT1= 6.5*m;  //diameter 10. m
  G4double deltaZ_SHAFT1=54.*m; // altezza arbitraria da cambiare 57m
  G4double startPhi_SHAFT1 =0.0*deg;
  G4double deltaPhi_SHAFT1=360.0*deg;

  G4double pos_x_SHAFT1 = -13.2*m;
  G4double pos_y_SHAFT1 = 0.*m;
  G4double Shaft_contingency=H_cavern/2;

  G4double Rmax_SHAFT2= 9.*m;  //diameter 15. m
  G4double deltaZ_SHAFT2=54.*m; // altezza arbitraria da cambiare
  G4double startPhi_SHAFT2 =0.0*deg;
  G4double deltaPhi_SHAFT2=360.0*deg;

  G4double pos_x_SHAFT2 = 13.2*m;
  G4double pos_y_SHAFT2 = 0.*m;

  // Create Solid
  G4VSolid* solidCavern =new G4Box("SolidCavern",L_cavern/2,W_cavern/2,H_cavern/2);
  G4Tubs* solidSHAFT1 = new G4Tubs("solidSHAFT1",0.,Rmax_SHAFT1,(deltaZ_SHAFT1+Shaft_contingency)/2,startPhi_SHAFT1,deltaPhi_SHAFT1);
  G4Tubs* solidSHAFT2 = new G4Tubs("solidSHAFT2",0.,Rmax_SHAFT2,(deltaZ_SHAFT2+Shaft_contingency)/2,startPhi_SHAFT2,deltaPhi_SHAFT2);

  solidCavern=new G4UnionSolid("SolidCavern",solidCavern,solidSHAFT1,0,G4ThreeVector(pos_x_SHAFT1,pos_y_SHAFT1,(deltaZ_SHAFT1+Shaft_contingency)/2.));
  solidCavern=new G4UnionSolid("SolidCavern",solidCavern,solidSHAFT2,0,G4ThreeVector(pos_x_SHAFT2,pos_y_SHAFT2,(deltaZ_SHAFT2+Shaft_contingency)/2.));

  //Create Logical
  G4LogicalVolume* logicCavern = new G4LogicalVolume(solidCavern,Concrete,"LogicCavern");
  logicCavern->SetVisAttributes(colorgray);



  // Create Physical (Placement)

  G4double posx_Cavern = 0.*m;
  G4double posy_Cavern = 0.*m;
  G4double posz_Cavern = deltaZ_SHAFT2+H_cavern/2;


  G4ThreeVector pos_Cavern = G4ThreeVector(posx_Cavern,posy_Cavern,-posz_Cavern+H_Rock);

  G4VPhysicalVolume* physicalCavern =nullptr;
  physicalCavern =new G4PVPlacement(0,     //no rotation
                      pos_Cavern,
                      logicCavern,            //its logical volume
                      "PhysicalCavern",               //its name
                      logicRock,            //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      checkOverlaps);        //overlaps checking

/*
  //Cavern int (inside cavern)

  G4double L_cavern_int=51.*m;
  G4double W_cavern_int=28.*m;
  G4double H_cavern_int=33.*m;


  // Create Solid
  G4VSolid* solidCavern_int =new G4Box("SolidCavern_int",L_cavern_int/2,W_cavern_int/2,H_cavern_int/2);

  //Create Logical
  G4LogicalVolume* logicCavern_int = new G4LogicalVolume(solidCavern_int,AIR,"LogicCavern_int");
  logicCavern_int->SetVisAttributes(colorcyan);

  // Create Physical (Placement)

  G4double posx_Cavern_int = 0.*m;
  G4double posy_Cavern_int = 0.*m;
  G4double posz_Cavern_int = 0.*m;

  G4ThreeVector pos_Cavern_int = G4ThreeVector(posx_Cavern_int,posy_Cavern_int,posz_Cavern_int);

  G4VPhysicalVolume* physicalCavern_int=nullptr;
  physicalCavern_int = new G4PVPlacement(0,     //no rotation
                      pos_Cavern_int,
                      logicCavern_int,            //its logical volume
                      "PhysicalCavern_int",               //its name
                      logicCavern,            //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      checkOverlaps);        //overlaps checking


  // *******************************
  //Create SHAFT1 (PX16)
  // *******************************


  // Create Solid

  G4double Rmin_SHAFT1 = 5.5*m;
  G4double Rmax_SHAFT1= 6.5*m;  //diameter 10. m
  G4double deltaZ_SHAFT1=57.*m; // altezza arbitraria da cambiare 57m
  G4double startPhi_SHAFT1 =0.0*deg;
  G4double deltaPhi_SHAFT1=360.0*deg;



  G4Tubs* solidSHAFT1 = new G4Tubs("solidSHAFT1",Rmin_SHAFT1,Rmax_SHAFT1,deltaZ_SHAFT1/2,startPhi_SHAFT1,deltaPhi_SHAFT1);

  //Create Logical
  G4LogicalVolume* logicalSHAFT1 = new G4LogicalVolume(solidSHAFT1,Concrete,"LogicalShaft1");
  logicalSHAFT1->SetVisAttributes(colorgray);

  //Create Physical

  G4double pos_x_SHAFT1 = -13.2*m;
  G4double pos_y_SHAFT1 = 0.*m;
  G4double pos_z_SHAFT1 = 18.0*m;//17.5*m;


  G4ThreeVector pos_SHAFT1 = G4ThreeVector(pos_x_SHAFT1,pos_y_SHAFT1,pos_z_SHAFT1);

  G4VPhysicalVolume* physicalSHAFT1=nullptr;
  physicalSHAFT1= new G4PVPlacement (0,pos_SHAFT1,logicalSHAFT1,"physicalSHAFT1",logicRock,false,0,checkOverlaps);

  // Da inserire un tubo di Aria di raggio 0 e 5.5

  G4double Rmin_SHAFT1_int = 0.*m;
  G4double Rmax_SHAFT1_int= 5.5*m;  //diameter 10. m
  G4double deltaZ_SHAFT1_int=57.*m; // altezza arbitraria da cambiare 57m
  G4double startPhi_SHAFT1_int =0.0*deg;
  G4double deltaPhi_SHAFT1_int=360.0*deg;

  G4double pos_x_SHAFT2 = 13.2*m;
  G4double pos_y_SHAFT2 = 0.*m;
  G4double pos_z_SHAFT2 = 18.0*m;//17.5


  G4Tubs* solidSHAFT1_int = new G4Tubs("solidSHAFT1_int",Rmin_SHAFT1_int,Rmax_SHAFT1_int,deltaZ_SHAFT1_int/2,startPhi_SHAFT1_int,deltaPhi_SHAFT1_int);

  //Create Logical
  G4LogicalVolume* logicalSHAFT1_int = new G4LogicalVolume(solidSHAFT1_int,AIR,"LogicalShaft1_int");
  logicalSHAFT1_int->SetVisAttributes(colorcyan);

  //Create Physical

  G4double pos_x_SHAFT1_int = 0.*m;
  G4double pos_y_SHAFT1_int = 0.*m;
  G4double pos_z_SHAFT1_int = 0.*m;//17.5*m;

  G4ThreeVector pos_SHAFT1_int = G4ThreeVector(pos_x_SHAFT1_int,pos_y_SHAFT1_int,pos_z_SHAFT1_int);

  G4VPhysicalVolume* physicalSHAFT1_int= nullptr;
  physicalSHAFT1_int=new G4PVPlacement (0,pos_SHAFT1_int,logicalSHAFT1_int,"physicalSHAFT1_int",logicalSHAFT1,false,0,checkOverlaps);

  // *******************************
  //Create SHAFT2 (PX14)
  // *******************************


  // Create Solid

  G4double Rmin_SHAFT2 = 8.*m;
  G4double Rmax_SHAFT2= 9.*m;  //diameter 15. m
  G4double deltaZ_SHAFT2=54.*m; // altezza arbitraria da cambiare
  G4double startPhi_SHAFT2 =0.0*deg;
  G4double deltaPhi_SHAFT2=360.0*deg;

  G4Tubs* solidSHAFT2 = new G4Tubs("solidSHAFT2",Rmin_SHAFT2,Rmax_SHAFT2,deltaZ_SHAFT2/2,startPhi_SHAFT2,deltaPhi_SHAFT2);

  //Create Logical
  G4LogicalVolume* logicalSHAFT2 = new G4LogicalVolume(solidSHAFT2,Concrete,"logicalSHAFT2");
  logicalSHAFT2->SetVisAttributes(colorgray);

  //Create Physical

  G4double pos_x_SHAFT2 = 13.2*m;
  G4double pos_y_SHAFT2 = 0.*m;
  G4double pos_z_SHAFT2 = 18.0*m;//17.5


  G4ThreeVector pos_SHAFT2 = G4ThreeVector(pos_x_SHAFT2,pos_y_SHAFT2,pos_z_SHAFT2);

  G4VPhysicalVolume* physicalSHAFT2=nullptr;
  physicalSHAFT2=new G4PVPlacement (0,pos_SHAFT2,logicalSHAFT2,"physicalSHAFT2",logicRock,false,0,checkOverlaps);


  // Da inserire un tubo di Aria di raggio 0 e 5.5

  G4double Rmin_SHAFT2_int = 0.*m;
  G4double Rmax_SHAFT2_int= 8.0*m;  //diameter 10. m
  G4double deltaZ_SHAFT2_int=54.*m; // altezza arbitraria da cambiare 57m
  G4double startPhi_SHAFT2_int =0.0*deg;
  G4double deltaPhi_SHAFT2_int=360.0*deg;


  G4Tubs* solidSHAFT2_int = new G4Tubs("solidSHAFT2_int",Rmin_SHAFT2_int,Rmax_SHAFT2_int,deltaZ_SHAFT2_int/2,startPhi_SHAFT2_int,deltaPhi_SHAFT2_int);

  //Create Logical
  G4LogicalVolume* logicalSHAFT2_int = new G4LogicalVolume(solidSHAFT2_int,AIR,"LogicalShaft2_int");
  logicalSHAFT2_int->SetVisAttributes(colorcyan);

  //Create Physical

  G4double pos_x_SHAFT2_int = 0.*m;
  G4double pos_y_SHAFT2_int = 0.*m;
  G4double pos_z_SHAFT2_int = 0.*m;//17.5*m;

  G4ThreeVector pos_SHAFT2_int = G4ThreeVector(pos_x_SHAFT2_int,pos_y_SHAFT2_int,pos_z_SHAFT2_int);

  G4VPhysicalVolume* physicalSHAFT2_int=nullptr;
  physicalSHAFT2_int = new G4PVPlacement (0,pos_SHAFT2_int,logicalSHAFT2_int,"physicalSHAFT2_int",logicalSHAFT2,false,0,checkOverlaps);



  // *******************************
  //Create ATLAS
  // *******************************


  // Create Solid

  G4double Rmin_ATLAS = 10.*m;
  G4double Rmax_ATLAS= 11.*m;  //diameter 11. m
  G4double deltaZ_ATLAS=21.5*m; // altezza 43
  G4double startPhi_ATLAS =0.0*deg;
  G4double deltaPhi_ATLAS=360.0*deg;

  G4Tubs* solidATLAS = new G4Tubs("solidATLAS",Rmin_ATLAS,Rmax_ATLAS,deltaZ_ATLAS,startPhi_ATLAS,deltaPhi_ATLAS);



  //Create Logical
  G4LogicalVolume* logicalATLAS = new G4LogicalVolume(solidATLAS,Concrete,"logicalATLAS");
  logicalATLAS->SetVisAttributes(colorgreen);


  //Create Physical

  G4double pos_x_ATLAS = 0.*m;
  G4double pos_y_ATLAS = 0.*m;
  G4double pos_z_ATLAS = -6.5*m; //old -35.*m


  G4ThreeVector pos_ATLAS = G4ThreeVector(pos_x_ATLAS,pos_y_ATLAS,pos_z_ATLAS);

  //G4RotationMatrix* zRot = physVol->GetRotation();
 G4RotationMatrix* yRot = new G4RotationMatrix;
 yRot->rotateY(M_PI/2.*rad);



  G4VPhysicalVolume* physicalATLAS=nullptr;
  physicalATLAS= new G4PVPlacement (yRot,pos_ATLAS,logicalATLAS,"physicalATLAS",logicCavern,false,0,checkOverlaps);


  // *******************************
  // Create Envelope
  // *******************************



  // stesse dimensioni Rock

  G4double env_sizeX=92.*m;
  G4double env_sizeY=92.*m;
  G4double env_sizeZ=92.*m;

  G4Material* env_mat = nist->FindOrBuildMaterial("G4_AIR");

  // Create Solid
  G4Box* solidEnv =new G4Box("Envelope",env_sizeX/2, env_sizeY/2, env_sizeZ/2);

  // Create Logical
  G4LogicalVolume* logicEnv =new G4LogicalVolume(solidEnv,env_mat,"Envelope");

  G4double posx_Env = 0.*m;
  G4double posy_Env = 0.*m;
  G4double posz_Env = 0.*m;

  G4ThreeVector pos_Env = G4ThreeVector(posx_Env,posy_Env,posz_Env);

  // Create Physical (Placement)
  new G4PVPlacement(0,                       //no rotation
                    pos_Env,
                    logicEnv,                //its logical volume
                    "Envelope",              //its name
                    logicRock,              //its mother  volume
                    false,                   //no boolean operation
                    0,                       //copy number
                    checkOverlaps);          //overlaps checking


  */



  if (writeGDML)
  {
	const std::string fileName="cosmicsGeometry.gdml";
        gdmlParser.Write(fileName,physicalWorld);
  }

  //
  //always return the physical World
  //
  return physicalWorld;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
