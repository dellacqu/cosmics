#ifndef CorsikaGenerator_H
#define CorsikaGenerator_H

#include <fstream>

class CorsikaGenerator {
public:
	static CorsikaGenerator* getInstance() {
		static CorsikaGenerator* theInstance=new CorsikaGenerator();
		return theInstance;
	}
	std::string getEvent() {
		std::string s;
		std::getline(ifs,s);
		ofs<<s<<std::endl;
		return s;
	}

private:
	CorsikaGenerator() {
		if (!ifs.is_open())
  		{	
    			ifs.open("corsika.txt",std::ifstream::in);
    			// open check_corsika file
    			if (!ofs.is_open()) ofs.open("check_corsika.txt");

    			std::string linein;
    			// read the header and print it out
    			std::cout<<"\n\n"<<std::endl;
    			for (int i=0;i<3;i++)
    			{
      				std::getline(ifs,linein);
      				std::cout<<"+++ "<<linein<<std::endl;
      				ofs<<"+++ "<<linein<<std::endl;
    			}
    			std::cout<<"\n\n"<<std::endl;
  		}

	}
	std::ifstream ifs;
        std::ofstream ofs;

};


#endif
